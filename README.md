# sqlitebrowser Docker image

## What

[sqlitebrowser](https://sqlitebrowser.org/) in a docker image!

## Why

Quoting the [project's `README`](https://github.com/sqlitebrowser/sqlitebrowser#debian):

> Note that Debian focuses more on stability rather than newest features.
> Therefore packages will typically contain some older (but well tested)
> version, compared to the latest release.

This image provides (as of writing) `sqlitebrowser-3.11.2-1build2`
from [`ubuntu:focal`](https://packages.ubuntu.com/focal/sqlitebrowser).

See versions across package repositories at [Repology](https://repology.org/project/sqlitebrowser/versions).

## How

```shell
docker run -d \
    -e DISPLAY=unix$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v $PWD:/sqlite \
    --name sqlitebrowser \
    letompouce/sqlitebrowser
```

### Configuration

A default `sqlitebrowser.conf` is provided, which points `defaultlocation`
 to `/sqlite` into the container.

You may want to mount your own `$HOME/.config/sqlitebrowser/sqlitebrowser.conf`
to `/root/.config/sqlitebrowser/sqlitebrowser.conf`.

### Troubleshooting

If it fails like this:

```text
QStandardPaths: XDG_RUNTIME_DIR not set, defaulting to '/tmp/runtime-root'
No protocol specified
qt.qpa.screen: QXcbConnection: Could not connect to display unix:0.0
Could not connect to any X display
```

You need to allow Docker to access your `DISPLAY`:

```shell
$ xhost + local:docker
non-network local connections being added to access control list
```

Fine tuning this command is left as an exercise to the reader.

## Where

* Source code: <https://gitlab.com/l3tompouce/docker/sqlitebrowser>
* Docker Image: <https://hub.docker.com/r/letompouce/sqlitebrowser>
